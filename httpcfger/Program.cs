﻿using System;

namespace httpcfger
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                Console.Write("Enter command (a for add, d for delete): ");

                var command = Console.ReadKey();

                Console.WriteLine();
                Console.Write("Enter lower port range: ");

                int lowerPort = Convert.ToInt32(Console.ReadLine());

                Console.Write("Enter upper port range: ");
                int upperPort = Convert.ToInt32(Console.ReadLine());

                var helper = new NetshHelper();

                for (int i = lowerPort; i <= upperPort; i++)
                {
                    if (command.Key == ConsoleKey.A)
                        helper.GrantPermission(string.Format("http://+:{0}/Subscriber", i));
                    else if (command.Key == ConsoleKey.D)
                        helper.DeletePermission(string.Format("http://+:{0}/Subscriber", i));
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }
    }
}