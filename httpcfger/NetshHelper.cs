﻿using System;
using System.Diagnostics;
using System.Security.Principal;

namespace httpcfger
{
    public class NetshHelper
    {
        public string User { get; set; }

        public NetshHelper()
        {
            User = new SecurityIdentifier("S-1-1-0").Translate(typeof (NTAccount)).ToString();
        }

        public void GrantPermission( string url )
        {
            var info = new ProcessStartInfo("netsh", string.Format(@"http add urlacl url={0} user=\{1}", url, User))
                {
                    CreateNoWindow = false,
                    UseShellExecute = false,
                };

            var p = new Process {StartInfo = info, EnableRaisingEvents = true};
            p.OutputDataReceived += (sender, e) => Console.WriteLine(e.Data);

            Console.WriteLine(info.FileName + " " + info.Arguments);
            p.Start();
            p.WaitForExit();
        }

        public void DeletePermission(string url)
        {
            var info = new ProcessStartInfo("netsh", string.Format(@"http delete urlacl url={0}", url))
            {
                CreateNoWindow = false,
                UseShellExecute = false,
            };

            var p = new Process {StartInfo = info, EnableRaisingEvents = true};
            p.OutputDataReceived += (sender, e) => Console.WriteLine(e.Data);

            Console.WriteLine(info.FileName + " " + info.Arguments);
            p.Start();
            p.WaitForExit();
        }
    }
}